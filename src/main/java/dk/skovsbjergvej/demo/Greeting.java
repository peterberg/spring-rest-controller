package dk.skovsbjergvej.demo;

public class Greeting {

  private final long sequence;
  private final String content;

  public Greeting(long sequence, String content) {
    this.sequence = sequence;
    this.content = content;
  }

  public long getSequence() {
    return sequence;
  }

  public String getContent() {
    return content;
  }
}
