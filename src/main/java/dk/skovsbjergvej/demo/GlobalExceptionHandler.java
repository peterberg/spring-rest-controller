package dk.skovsbjergvej.demo;

import dk.skovsbjergvej.demo.exception.*;

import reactor.core.publisher.Mono;

import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.core.io.buffer.DataBuffer;

import reactor.core.publisher.Flux;
import java.nio.charset.StandardCharsets;

@Order(-2)
@RestControllerAdvice
public class GlobalExceptionHandler implements ErrorWebExceptionHandler {

  private static final String NOT_FOUND = "Resource not found";
  private static final String INTERNAL_SERVER_ERROR = "An unforeseen error occurred";

  @ExceptionHandler(BadRequestException.class)
  public ResponseEntity<String> handle(BadRequestException e) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).contentType(MediaType.TEXT_PLAIN).body(e.getMessage());
  }

  @ExceptionHandler(NotFoundException.class)
  public ResponseEntity<String> handle(NotFoundException e) {
    return ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(MediaType.TEXT_PLAIN).body(e.getMessage());
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<String> handle(Exception e) {
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).contentType(MediaType.TEXT_PLAIN)
        .body(e.getMessage());
  }

  @Override
  public Mono<Void> handle(ServerWebExchange exchange, Throwable e) {
    if (exchange.getResponse().isCommitted()) {
      return Mono.error(e);
    }

    String message;

    if (e instanceof ResponseStatusException) {
      exchange.getResponse().setStatusCode(((ResponseStatusException) e).getStatus());


      switch (((ResponseStatusException) e).getStatus()) {
      case NOT_FOUND:
        message = NOT_FOUND;
        break;
      default:
        message = INTERNAL_SERVER_ERROR;
      }
    } else {
      exchange.getResponse().setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR);
      message = INTERNAL_SERVER_ERROR;
    }

    byte[] bytes = message.getBytes(StandardCharsets.UTF_8);
    DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(bytes);
    return exchange.getResponse().writeWith(Flux.just(buffer));
  }
}