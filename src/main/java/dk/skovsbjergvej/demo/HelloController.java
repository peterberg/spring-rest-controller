package dk.skovsbjergvej.demo;

import dk.skovsbjergvej.demo.exception.BadRequestException;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class HelloController {
  private static final String template = "Hello, %s!";

  private final AtomicLong counter = new AtomicLong();

  @RequestMapping(method = RequestMethod.GET, value = "/hello")
  public Mono<Greeting> get(@RequestParam(value = "name", defaultValue = "World") String name) {
    if (name.equals("error")) {
      throw new BadRequestException("The name 'error' is not allowed.");
    }
    if(name.equals("null")) {
      throw new NullPointerException("Name was 'null'.");
    }

    return Mono.just(new Greeting(counter.incrementAndGet(), String.format(template, name)));
  }
}
