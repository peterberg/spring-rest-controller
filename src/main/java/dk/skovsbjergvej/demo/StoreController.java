package dk.skovsbjergvej.demo;

import dk.skovsbjergvej.demo.exception.NotFoundException;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import reactor.core.publisher.Mono;

import com.fasterxml.jackson.databind.node.ObjectNode;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/store")
public class StoreController {
  private final Map<UUID, ObjectNode> store = new HashMap<UUID, ObjectNode>();

  /**
   *
   * @param key
   * @return
   */

  @RequestMapping(value = "/{key}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public Mono<ObjectNode> get(@PathVariable(value = "key") UUID key) {
    ObjectNode result = store.get(key);

    if (result == null) {
      throw new NotFoundException();
    }

    return Mono.just(result);
  }

  /**
   *
   */

  @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<Mono<ObjectNode>> post(@RequestBody ObjectNode payload) throws URISyntaxException {
    UUID key = UUID.randomUUID();

    store.put(key, payload);

    return ResponseEntity.created(new URI(key.toString())).body(Mono.just(payload));
  }

  /**
   *
   */

  @RequestMapping(value = "/{key}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public Mono<ObjectNode> put(@PathVariable(value = "key") UUID key, @RequestBody ObjectNode payload) {
    ObjectNode previous = store.put(key, payload);

    if (previous == null) {
      throw new NotFoundException();
    }

    return Mono.just(payload);
  }

  /**
   *
   * @param key
   * @return
   */

  @RequestMapping(value = "/{key}", method = RequestMethod.DELETE)
  public ResponseEntity<Void> delete(@PathVariable(value = "key") UUID key) {
    ObjectNode previous = store.remove(key);

    if (previous == null) {
      throw new NotFoundException();
    }

    return ResponseEntity.noContent().build();
  }
}
