FROM maven:3.6.0-jdk-12-alpine AS Build

WORKDIR /workdir
COPY pom.xml .
RUN mvn dependency:go-offline
COPY . .
RUN mvn package

FROM openjdk:12-alpine AS Serve
EXPOSE 8080

WORKDIR /opt/app
COPY --from=Build /workdir/target/spring-rest-controller-0.0.1.jar .
CMD java -jar spring-rest-controller-0.0.1.jar