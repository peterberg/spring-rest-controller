# HelloWorld Spring Boot REST Controller

## To edit in VS Code
```
code .
```

## Build using maven (requires JDK 11)
```
mvn clean install
```

## Run outside VS Code
```
java -jar target/spring-rest-controller-0.0.1.jar
```
Test it using curl:
```
curl -X GET localhost:8080/hello
```

## Build Docker image and run it in a container
```
export DOCKER_HOST=localhost
docker image build -t spring-rest-controller .
docker container run -p 8080:8080 --name spring-rest-controller spring-rest-controller
```
Stop the running container:
```
docker container stop spring-rest-controller
```
Restart the stopped container:
```
docker container start spring-rest-controller
```
Remove the stopped container in order to be able to rerun it, possibly from a rebuilt image:
```
docker container rm spring-rest-controller
```

